const express = require('express');
const mongoose = require('mongoose');
const app = express();
 
require('./models/home');
const Home = mongoose.model('Home');

mongoose.connect('mongodb://localhost:27017/myapp', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log("Conexão com o MongoDB realizado com sucesso")
}).catch((erro) => {
  console.log("Erro: conexão com o MongoDB não realizada   " + erro)
});

app.get("/home", (req,res) => {
  return res.json({
    erro: false,
    mensage: "Informação da página home"
  });
});

app.post("/home", (req, res) => {
  Home.create(req.body, (err) => {
    if(err) return res.status(400).json({
      erro: true,
      massage: "erro: Conteudo da página home não cadastrado com sucesso"
    });
  });

  return res.json({
    erro: false,
    mensage: "Conteudo da página home cadastrada com sucesso!"
  })
});

app.listen(8080,() => {
    console.log("Servidor iniciado na porta 8080: http://localhost:8080/")
});